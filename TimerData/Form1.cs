﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimerData
{
    public partial class Form1 : Form
    {
        DateTime dataHora;
        DateTime dataHoraTimer;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataHora = DateTime.Now;
            label1.Text = dataHora.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            dataHoraTimer = DateTime.Now;
            label2.Text = "Data: " + dataHoraTimer.ToLongDateString() + " Hora: " + dataHoraTimer.ToLongTimeString();
        }
    }
}
